import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContestComponent} from "./logistics/contest/contest.component";
import {StatisticsComponent} from "./statistics/statistics.component";
import {RobotControllerComponent} from "./robot-controller/robot-controller.component";

const routes: Routes = [
  {path: '', redirectTo: 'contest', pathMatch: 'full'},
  {path: 'contest', component: ContestComponent},
  {path: 'statistics', component: StatisticsComponent},
  {path: 'robot-controller', component: RobotControllerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
