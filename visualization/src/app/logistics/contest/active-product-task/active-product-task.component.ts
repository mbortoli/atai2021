import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-active-product-task',
  templateUrl: './active-product-task.component.html',
  styleUrls: ['./active-product-task.component.sass']
})
export class ActiveProductTaskComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['id', 'name', 'machine', 'robotId'];
  @Input() data;
  @Input() shrink;

  dataSource: MatTableDataSource<any>;

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>();
    this.dataSource.data = this.data.subProductionTasks;
  }

  ngAfterViewInit(): void {
  }

}
