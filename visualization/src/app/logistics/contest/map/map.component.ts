import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import * as Konva from 'konva/lib';

interface NodeWithText {
  circle: Konva.Node,
  label: Konva.Text,
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})

export class MapComponent implements OnInit, AfterViewInit {

  private offsetYForHeader = 300;
  private robotLayerOffsetY = 800;

  @Input() data;
  @Input() robot;

  stage: Konva.Stage;

  machineLayer: Konva.Layer;
  robotLayer: Konva.Layer;

  robotsKonvaNodes: NodeWithText[];

  constructor() {
    this.robotsKonvaNodes = [];
    this.robotsKonvaNodes.push(this.robotToCircle(0, 1, 5));
    this.robotsKonvaNodes.push(this.robotToCircle(1, 2, 5));
    this.robotsKonvaNodes.push(this.robotToCircle(2, 3, 5));
  }

  ngOnInit() {
    console.log(this.robot);
    this.machineLayer = new Konva.Layer({
      offsetX: -800,
      offsetY: window.innerHeight - this.offsetYForHeader,
      scaleY: -1,
    });
    this.robotLayer = new Konva.Layer({
      offsetX: 0,
      offsetY: -1 * this.robotLayerOffsetY,
      scaleY: 1,
      scaleX: 1,
    });
    this.robotsKonvaNodes.forEach(node => this.robotLayer.add(node.circle));
    this.robotsKonvaNodes.forEach(node => this.robotLayer.add(node.label));
  }

  ngAfterViewInit(): void {
    this.stage = new Konva.Stage({
      scrollable: true,
      container: 'container',
      width: window.innerWidth - 50,
      height: window.innerHeight -this.offsetYForHeader
    });

    this.stage.add(this.machineLayer);
    this.stage.add(this.robotLayer);

    let filteredData = this.data.filter(x => x.machine != null);


    console.log(this.data);
    console.log("filtered", filteredData);

    //data is based in Centimeters!
    filteredData.forEach(machine => {
      console.log("Machine placing at x", machine.zoneCenter.x, "y", machine.zoneCenter.y);
      console.log(machine);
      const team = machine.machine.split("-")[0];
      const name = machine.machine.split("-")[1];
      console.log("team is, ", team);
      let color = 'cyan';
      if (team == 'M') {
        color = 'magenta';
      }
      var square:NodeWithText = {
        circle: new Konva.Rect({
          x: machine.zoneCenter.x * 100,
          y: machine.zoneCenter.y * 100,
          width: 100,
          height: 50,
          fill: color,
          stroke: 'black',
          strokeWidth: 4
        }),
        label: new Konva.Text({
          text: name,
          x: machine.zoneCenter.x * 100 + 10,
          y: machine.zoneCenter.y * 100 + 45,
          fontFamily: 'Calibri',
          scaleY: -1,
          fontSize: 30,
          padding: 5,
          fill: 'black'
        })};
      this.machineLayer.add(square.circle);
      this.machineLayer.add(square.label);
    });

    this.machineLayer.draw();
    this.robotLayer.draw();
    this.update();
    this.stage.on('click', () => this.onLeftClick());
    console.log(this.robot);
  }

  onLeftClick() {
    console.log("Left clicked!!");
    console.log("robots: ", this.robot);
    this.update();
  }

  public update() {
    console.log("rob: ", this.robot);
    this.robot.forEach(rob => {
      if (rob != null) {
        console.log(rob.robotId);
        this.robotsKonvaNodes[rob.robotId - 1].circle.absolutePosition({
          x: rob.poseX * 100 + 850,
          y: -1 * rob.poseY * 100 + this.robotLayerOffsetY
        });
        this.robotsKonvaNodes[rob.robotId - 1].label.absolutePosition({
          x: rob.poseX * 100 + 850 - 20,
          y: -1 * rob.poseY * 100 + this.robotLayerOffsetY + 25
        });
        console.log(this.robotsKonvaNodes[rob.robotId - 1]);
      }
    });
    console.log("redrawing robots", this.robotsKonvaNodes);
    this.robotLayer.draw();
  }

  robotToCircle(index, x, y): NodeWithText {
    console.log("creating circle for text")
    let color = "brown";
    if (index == 1) {
      color = "blue";
    } else if (index == 2) {
      color = "green";
    }
    return {
      circle: new Konva.Circle({
      x: x * 100,
      y: y * 100,
      radius: 25,
      fill: color,
      stroke: 'black',
      strokeWidth: 4
    }),
    label: new Konva.Text({
      text: "" + index,
      x: x * 100 -20,
      y: y * 100 +25,
      scaleY: -1,
      fontFamily: 'Calibri',
      fontSize: 50,
      padding: 5,
      fill: 'black'
    })};
  }


}
