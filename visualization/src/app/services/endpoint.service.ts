import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  constructor(private httpClient: HttpClient) { }

  public post(url: string, payload: any) {
    return this.httpClient.post("http://localhost:8090/" + url, payload);
  }
}
