package com.grips.scheduler.asp.planner;


import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.grips.scheduler.asp.encoding.CodedProblem;
import com.grips.scheduler.asp.encoding.IntOp;
import com.grips.scheduler.asp.planparser.TemporalEdge;
import com.grips.scheduler.asp.util.TemporalPlan;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.tomcat.jni.Proc;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;

@CommonsLog
public class Planner {

    private String domain;
    private String problem;
    private TemporalPlan plan;
    private CodedProblem cp;
    private final String plannerBinary = "./optic";

    private String [] oldRobotPosition;

    public Planner(String domain, String problem, CodedProblem cp){
        this.domain = domain;
        this.problem = problem;
        this.cp = cp;
        plan = new TemporalPlan();
        oldRobotPosition = new String[3];


    }

    public void plan (){

        this.problem = "problem.pddl";

        String s;
        Process p = null;
        boolean parseAction = false;
        boolean parseTime = false;
        boolean finish = false;

        //swapping robotIds of the plan such that first action is executed by R1, second action by R2, ecc.."
        int checkedRobots = 0;
        int [] robotMap = new int[3];
        robotMap[0] = -1;
        robotMap[1] = -1;
        robotMap[2] = -1;

        try {
            p = Runtime.getRuntime().exec(plannerBinary + " " + domain + " " + problem);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            List<IntOp> op = cp.getOperators();
            List<String> constants = cp.getConstants();
            boolean skipline = false;

            Timer timer;
            timer = new Timer();
            timer.schedule(new PlanningTimeout(p), 60000);

            while ((s = br.readLine()) != null && !finish ) {
                //System.out.println(s);
                String[] tokens = s.split(" ");
                if (tokens.length > 3)
                    if (tokens[1].equals("Plan") && tokens[2].equals("found") )
                        System.out.println("plan found");
                if (tokens.length > 2 && parseTime)
                    if (!tokens[1].equals("Dummy"))
                        parseAction = true;
                if (parseAction) {
                    if (s.equals("")) {
                        parseAction = false;
                        finish = true;
                    }
                    else {
                        String [] parsed = s.split(":");
                        double time = Double.parseDouble(parsed[0]);
                        int actionIndex = parsed[1].indexOf(")") + 1;
                        String action = parsed[1].substring(1,actionIndex);
                        double duration = Double.parseDouble( parsed[1].substring(actionIndex + 3, parsed[1].length() - 1) );
                        String actionName = action.split(" ")[0].substring(1);
                        //adding grounded actions with time and duration
                        if(!actionName.equals("waitfordeliverywindow")) {           //this is a toy action to encode temp constraint, not a real action
                            skipline = false;
                            Iterator<IntOp> it = op.iterator();
                            IntOp grAction = null;
                            while (it.hasNext()) {
                                IntOp i = it.next();
                                if (i.getName().equals(actionName)) {
                                    grAction = new IntOp(i);
                                    String[] paramParsing = action.substring(0, action.length() - 1).split(" ");
                                    String[] parameters = Arrays.copyOfRange(paramParsing, 1, paramParsing.length);
                                    for (int j = 0; j < grAction.getInstantiations().length; j++) {
                                        if ((parameters[j].equals("r1") || parameters[j].equals("r2") || parameters[j].equals("r3"))) {
                                            int robotId = Character.getNumericValue(parameters[j].charAt(1));
                                            if (checkedRobots < 3 && robotMap[robotId - 1] == -1) {
                                                robotMap[robotId - 1] = checkedRobots;
                                                checkedRobots++;
                                            }
                                            grAction.setValueOfParameter(j, constants.indexOf("r" + (robotMap[robotId - 1] + 1)));
                                        }  else  {
                                            grAction.setValueOfParameter(j, constants.indexOf(parameters[j]));
                                        }
                                    }
                                    grAction.setDuration(duration);
                                    plan.add(time, grAction);
                                }
                            }
                        } else
                            skipline = true;
                    }
                }
                if (tokens.length > 2 && !skipline) {
                    if (tokens[1].equals("Dummy") && tokens[2].equals("steps:"))
                        parseAction = true;
                    else if (tokens[1].equals("Time"))
                        parseTime = true;
                }
                System.out.println("line: " + s);
            }
            //p.waitFor();

            System.out.println("robots 1 is " + robotMap[0] + " robots 2 is " + robotMap[1] + "robots 3 is " + robotMap[2]);
            log.info("exit: " + p.exitValue());
            p.destroy();
        } catch (Exception e) { //log.error("Exception during planning call: " + e); e.printStackTrace();
            //since in this version I always use the first plan found and I terminate the process, I ignore the exception
            p.destroy(); }




    }


    public void printPlan () {
        TreeMap<Double, Set<IntOp>> actions = plan.actions();
        for (Map.Entry<Double, Set<IntOp>> entry : actions.entrySet()) {
            System.out.print("Time: " + entry.getKey() + " ");
            Set<IntOp> seti = entry.getValue();
            Iterator<IntOp> setIt = seti.iterator();
            while(setIt.hasNext()){
                IntOp i = setIt.next();
                System.out.print("name " + i.getName() + " ");
                for(int j=0; j < i.getInstantiations().length; j++)
                    System.out.print(i.getInstantiations()[j] + " ");
                System.out.print(" duration " + i.getDuration() + "\n");
            }
        }
        if (actions.size() == 0)
            System.out.print("++++++++++++++++EMPTY PLAN++++++++++++++++++");
    }

    public TemporalPlan getPlan () {
        return this.plan;
    }

    public class PlanningTimeout extends TimerTask {

        Process p;

        public PlanningTimeout(Process p) {
            this.p = p;
        }

        @Override
        public void run() {

            if (p.isAlive()) {
                log.warn("Planning out of time, interrupting");
                p.destroy();
            }
        }


    }


}
