package com.grips.scheduler.asp.planparser;

import java.util.HashMap;

public class TemporalNode {

    private int type; //1->action start 2->action end 3->plan start
    private String name;
    private int id;
    private int [] instantiation;
    private double time;
    private boolean valid; //if false, the node has been invalidated due to an order deleting

    private HashMap<Integer, Double> softDeadline;
    private HashMap<Integer, Double> hardDeadline;

    private int orderId;


    public TemporalNode(int type, String name, int id, double time) {
        this.type = type;
        this.name = name;
        this.id = id;
        this.time = time;
        this.valid = true;
        softDeadline = new HashMap<>();
        hardDeadline = new HashMap<>();
    }

    public void addDeadline (int key, double dl) {
        softDeadline.put(key,dl);
    }

    public void addHardDeadline (int key, double dl) {
        hardDeadline.put(key,dl);
    }

    public HashMap<Integer, Double> getSoftDeadline() {
        return softDeadline;
    }

    public HashMap<Integer, Double> getHardDeadline() {
        return hardDeadline;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int[] getInstantiation() {
        return instantiation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString () {
        return name;
    }

    public void setInstantiation(int[] instantiation) {
        this.instantiation = instantiation;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }
}
