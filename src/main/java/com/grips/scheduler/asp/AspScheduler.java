package com.grips.scheduler.asp;

import com.grips.config.ProductionConfig;
import com.grips.model.teamserver.*;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.*;
import com.grips.robot.RobotClientWrapper;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.asp.dispatcher.ActionMapping;
import com.grips.scheduler.asp.encoding.CodedProblem;
import com.grips.scheduler.asp.encoding.Encoder;
import com.grips.scheduler.asp.encoding.IntOp;
import com.grips.scheduler.asp.parser.Domain;
import com.grips.scheduler.asp.parser.Parser;
import com.grips.scheduler.asp.parser.Problem;
import com.grips.scheduler.asp.planner.Planner;
import com.grips.scheduler.asp.planparser.TemporalEdge;
import com.grips.scheduler.asp.planparser.TemporalGraph;
import com.grips.scheduler.asp.planparser.TemporalNode;
import com.grips.scheduler.asp.util.KnowledgeBase;
import com.grips.scheduler.asp.util.TempAtom;
import com.grips.scheduler.production.resouce_managment.ResourceDemandService;
import com.grips.scheduler.production.resouce_managment.ResourceLockService;
import com.grips.team_server.MPSHandler;
import com.grips.team_server.SubTaskGenerator;
import com.grips.tools.PathEstimator;
import com.robot_communication.services.RobotClient;
import com.shared.domain.Complexity;
import com.shared.domain.MachineSide;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static com.grips.persistence.domain.SubProductionTask.TaskState.*;

@CommonsLog
public class AspScheduler implements IScheduler {

    private String planner = "./optic";
    private String domainS = "domain.pddl";
    private String problemS = "problem";

    private final ProductOrderDao productOrderDao;
    private final ProductTaskDao productTaskDao;
    private final GameStateDao gameStateDao;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private final SubProductionTaskDao subProductionTaskDao;
    private final AtomDao atomDao;
    private final RobotClient robotClient;
    private final MachineClient client;
    private final MPSHandler mpsHandler;
    private final SubTaskGenerator subTaskGenerator;
    private final RingDao ringDao;
    private final BeaconSignalFromRobotDao beaconSignalFromRobotDao;
    private final ResourceDemandService resourceDemandService;
    private final ResourceLockService resourceLockService;
    private final RobotClientWrapper robotClientWrapper;
    private final PathEstimator pathEstimator;


    public static final String READY_AT_OUTPUT = "READY-AT-OUTPUT";
    public static final String IDLE = "IDLE";
    public static final String DOWN = "DOWN";
    public static final String BROKEN = "BROKEN";
    public static final String PREPARED = "PREPARED";
    public static final String PROCESSING = "PROCESSING";
    public static final String PROCESSED = "PROCESSED";
    @Value("${gameconfig.productiontimes.max_wait}")
    private int maxWait;

    @Autowired
    private ProductionConfig productionConfig = new ProductionConfig();

    private boolean first = true;

    CodedProblem cp;

    private Thread loop;

    private int minOrdersToPlan = 3;

    private List<ProductOrder> goals;


    private boolean stop;
    private boolean robotStopped[];


    private BigInteger currentTime;
    static long timerStarting[];
    private Timer timer;
    private TemporalGraph tgraph;
    private KnowledgeBase kb;
    private ActionMapping actionMapping;
    private Boolean starting;

    private HashMap<String, Double> distanceMatrix;
    private TemporalNode executing[];
    private SubProductionTask toExecute[];          //keep track of the assigned subProduction task, needed since move action are now executed explicitly
    private double timeFactor = 0.36;   //approximated map between plan times and real times (not the simulation real time factor)



    public AspScheduler (RingDao ringDao, SubTaskGenerator subTaskGenerator, RobotClient robotClient, MachineClient client, MPSHandler mpsHandler, ProductOrderDao productOrderDao, ProductTaskDao productTaskDao, GameStateDao gameStateDao, MachineInfoRefBoxDao machineInfoRefBoxDao,
                         SubProductionTaskDao subProductionTaskDao, AtomDao atomDao, BeaconSignalFromRobotDao beaconSignalFromRobotDao, ResourceDemandService resourceDemandService, ResourceLockService resourceLockService, RobotClientWrapper robotClientWrapper, PathEstimator pathEstimator) {
        this.ringDao = ringDao;
        this.subTaskGenerator = subTaskGenerator;
        this.client = client;
        this.robotClient = robotClient;
        this.mpsHandler = mpsHandler;
        this.productOrderDao = productOrderDao;
        this.productTaskDao = productTaskDao;
        this.gameStateDao = gameStateDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.subProductionTaskDao = subProductionTaskDao;
        this.atomDao = atomDao;
        this.beaconSignalFromRobotDao = beaconSignalFromRobotDao;
        this.loop = new Thread(this::runThread);
        this.loop.start();
        executing = new TemporalNode[3];
        robotStopped = new boolean [3];
        toExecute = new SubProductionTask [3];
        timerStarting = new long[3];
        robotStopped[0] = true;
        robotStopped[1] = true;
        robotStopped[2] = true;
        starting = true;
        stop = false;
        this.resourceDemandService = resourceDemandService;
        this.resourceLockService = resourceLockService;
        this.robotClientWrapper = robotClientWrapper;
        this.pathEstimator = pathEstimator;
    }


    @SneakyThrows
    private void runThread() {

    }


    @SneakyThrows
    public void calculatePlan () {
        this.kb = new KnowledgeBase(atomDao, productOrderDao, productTaskDao, gameStateDao, ringDao, machineInfoRefBoxDao, subProductionTaskDao, productionConfig);
        this.kb.generateInitialKB();
        generateDistanceMatrix();

        goals = new ArrayList<>();
        goals.add(goalSelector());  //calling the goal reasoner

        System.out.println("PLANNING");
        generateProblemFile(goals);
        ImmutablePair<TemporalGraph,CodedProblem> planningResult = null;
        try {
            planningResult = callExtPlanner();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.tgraph = planningResult.left;
        this.cp = planningResult.right;

        kb.setCp(cp);
        kb.generateOrderKb(this.goals);

    }


    private void generateGoalOnProblemFile(List<ProductOrder> goals, FileWriter fr) throws IOException {
        for (ProductOrder p : goals) {
            long id = p.getId();
            //manage the case in which the order was already present (replanning) (by doing nothing)
            if (atomDao.findByNameAndAttributes("complexity", "p" + id).isEmpty()) {
                fr.write(new Atom("stage_c0_0", "p" + id).prettyPrinter());
            }
        }
    }


    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal  beacon_signal, int robotId, Peer robot) {
        //THIS CODE IS A SHOW-CASE AND SHOULD BE CHANGED OR DELETED
        synchronized (this) {

            if (first && productOrderDao.count() >= minOrdersToPlan && robotId == 1) {  //robot 1 act as a controller which starts the plan
                first = false;
                calculatePlan();

                this.actionMapping = new ActionMapping(cp,productOrderDao, ringDao);
                dispatchPlan(kb);
            }



        }
    }

    public void dispatchPlan(KnowledgeBase kb) {
        currentTime = gameStateDao.getLatestGameTimeProductionPhase();
        dispatch();
    }

    public ProductOrder goalSelector () {
        List<ProductOrder> orders = (List<ProductOrder>) productOrderDao.findByDelivered(false);  //retrieving all the orders from the refbox
        return orders.get(0); //return the first found product

    }



    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {

        if (prsTask.hasGetFromStation() ) {
            this.handleGetResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());
        } else if (prsTask.hasDeliverToStation()) {
            this.handleDeliverResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getSuccessful());       //todo check how to get inf about holding product
        }  else if (prsTask.hasMoveToWaypoint()) {
            this.handleMoveToWaypointResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getMoveToWaypoint(), prsTask.getSuccessful());
        } else if (prsTask.hasBufferCapStation()) {
            this.handleBufferCapStationResult(prsTask.getTaskId(), prsTask.getRobotId(), prsTask.getBufferCapStation(), prsTask.getSuccessful());
        } else {
            log.error("Unsupportet midlevelTaskResult: " + prsTask.toString());
        }

    }

    private void handleBufferCapStationResult(int taskId, int robotId, GripsMidlevelTasksProtos.BufferCapStation bufferCapStation, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return;
        }

        if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.error("MoveTo Task: " + taskId + " was finished by robot: " + robotId);

            log.warn("SUCCESS FOR A BUFFER TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId-1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while(it.hasNext()){
                TemporalEdge e = it.next();
                if(e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }

        } else {
            log.error("MoveTo Buffer Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.warn("retrying with buffer task");
            assignTaskToRobot(toExecute[robotId-1],robotId);

        }
    }

    private void handleMoveToWaypointResult(Integer taskId, Integer robotId, GripsMidlevelTasksProtos.MoveToWaypoint moveToWaypoint, boolean wasSucess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);
        if (subTask == null) {
            log.error("Error, task with result was not found!");
            return;
        }

        if (wasSucess) {
            subTask.setState(SUCCESS, "robot said MoveToTask was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("MoveTo Task: " + taskId + " was finished by robot: " + robotId);

            if (toExecute[robotId-1] != null) {

                timerStarting[robotId - 1] = gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000;
                assignNonMoveTaskToRobot(toExecute[robotId-1],robotId);

            }else {
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId-1]);
                Iterator<TemporalEdge> it = outEdges.iterator();
                while(it.hasNext()){
                    TemporalEdge e = it.next();
                    if(e.getType() == 2) {
                        e.enable();
                        tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                        dispatchEndNode(tgraph.getTarget(e));
                    }
                }
            }
        } else {
            log.error("MoveTo Task Failed result: " + wasSucess + " for task: " + taskId + " unassigning it and retrying it...");
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);
            log.warn("Reassigning MOVE task");
            try {
                robotClientWrapper.sendProductionTaskToRobot(subTask);
            } catch (Exception e) {
                log.warn(e);
            }
        }
    }

    public void handleDeliverResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            System.err.println("Error, task that should fail was not found!");
            return;
        }

        if (wasSuccess) {

            subTask.setState(SUCCESS_PENDING, "robot said task was SUCCESS");
            subProductionTaskDao.save(subTask);
            log.info("Deliver Task: " + taskId + " was supposedly finished by robot: " + robotId);


            // no prepare required
            if (subTask.getMachine().contains("RS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);
            }

            if (subTask.getMachine().contains("DS")) {
                subTask.setState(SUCCESS, "deliver was done to slide");
                subProductionTaskDao.save(subTask);
                ProductOrder order = productOrderDao.findById(subTask.getOrderInfoId()).get();
                order.setDelivered(true);
                productOrderDao.save(order);
            }


            log.warn("SUCCESS FOR A DELIVERY TASK");

            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId-1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while(it.hasNext()){
                TemporalEdge e = it.next();
                if(e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }





        } else {
            log.error("Task Failed result: " + wasSuccess + " for task: " + taskId);
            subTask.setState(TBD, "Retrying after FAIL");
            subProductionTaskDao.save(subTask);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.warn("retrying with delivery task");
            assignTaskToRobot(toExecute[robotId-1],robotId);
        }
    }

    private void successProductTask(SubProductionTask subTask) {
        //resourceDemandService.clearDemands(subTask.getProductTask());
        subTask.getProductTask().setState(ProductTask.ProductState.FINISHED, "deliver was done");
        productTaskDao.save(subTask.getProductTask());
    }

    public void handleResourcesOnSuccess(SubProductionTask subTask) {
        if (!StringUtils.isEmpty(subTask.getIncrementMachine())) {
            resourceLockService.addNewParts(subTask.getIncrementMachine());
        }

        if (subTask.getDecrementCost() > 0) {
            resourceLockService.tryDecreaseParts(subTask.getDecrementMachine(), subTask.getDecrementCost(), subTask.getProductTask().getProductOrder().getId());
        }

    }

    public void handleGetResult(long taskId, int robotId, boolean wasSuccess) {
        SubProductionTask subTask = subProductionTaskDao.findByIdAndRobotId(taskId, robotId);

        if (subTask == null) {
            System.err.println("Error, task that should fail was not found!");
            return;
        }

        if (wasSuccess) {
            subTask.setState(SUCCESS, "robot said SUCCESS");
            log.warn("SUCCESS FOR GET task" + taskId);
            subProductionTaskDao.save(subTask);



            //todo manage successfull task
            Set<TemporalEdge> outEdges = tgraph.getOutputEdges(executing[robotId-1]);
            Iterator<TemporalEdge> it = outEdges.iterator();
            while(it.hasNext()){
                TemporalEdge e = it.next();
                if(e.getType() == 2) {
                    e.enable();
                    tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                    dispatchEndNode(tgraph.getTarget(e));
                }
            }



        } else {
            log.error("GET Task: " + taskId + " was failed by robot: " + robotId);
            if (subTask.getMachine().contains("BS")) {
                handleBsGetFail(subTask);
            } else if (subTask.getMachine().contains("CS") || subTask.getMachine().contains("RS")) {
                subTask.setState(FAILED);
                subProductionTaskDao.save(subTask);
                handleCsRsGetFail(subTask);
            } else {
                log.error("GET task at unexpected machine " + subTask.getMachine() + " taskId: " + taskId);
            }
        }
    }

    private void handleBsGetFail(SubProductionTask subTask) {
        client.sendResetMachine(MachineClientUtils.parseMachineWithColor(subTask.getMachine()));
        mpsHandler.stopPreparingMachine(subTask);
        System.err.println("get at BS failed");
    }



    private void handleCsRsGetFail(SubProductionTask subTask){
        if (MachineSide.SHELF.equals(subTask.getSide())) {
            System.err.println("get at SHELF failed");
        } else if (MachineSide.OUTPUT.equals(subTask.getSide())) {
            String currentMachineState = getCurrentMachineState(subTask);

            if (READY_AT_OUTPUT.equalsIgnoreCase(currentMachineState)) {
                System.err.println("task failed but machine still ready-at-output");
            } else if (IDLE.equalsIgnoreCase(currentMachineState)) {

                System.err.println("GET failed and machine is IDLE we probably pushed product in machine");
                Collection<SubProductionTask> successPendingAtInput = subProductionTaskDao.findByMachineAndSideInAndStateIn(subTask.getMachine(), Arrays.asList(MachineSide.INPUT), Arrays.asList(SUCCESS_PENDING));
                //if (successPendingAtInput == null || successPendingAtInput.size() == 0) {
                    // there is no other product at input but we failed, so to be sure: reset mps (we do not impact other product at input)
                    client.sendResetMachine(MachineClientUtils.parseMachineWithColor(subTask.getMachine()));
                    mpsHandler.stopPreparingMachine(subTask);


                    //check if cap is still present
                    if (subTask.getMachine().substring(2,4).equals("CS")) {
                        handleCSFail(subTask);
                    }
                    // empty machine parts
                    //resourceManager.clearAllParts(subTask.getMachine());
                //}

                // clear demand or fail product in any case
                if (!subTask.isDemandTask()) {
                    System.err.println("get product failed and product not at machine");
                }
            } else {
                System.err.println("Unexpected machinestate " + currentMachineState + " at machine " + subTask.getMachine());
            }
        } else {
            System.err.println("GET task FAILED for unexpected side " + subTask.getSide() + " at machine " + subTask.getMachine());
        }
    }

    private void handleCSFail(SubProductionTask subTask) {
        String machineName = subTask.getMachine();
        MachineInfoRefBox machine = machineInfoRefBoxDao.findByName(machineName);
    }


    private String getCurrentMachineState(SubProductionTask subTask) {
        MachineInfoRefBox mInfoRb = machineInfoRefBoxDao.findByName(subTask.getMachine());
        String machineState = IDLE;
        if (mInfoRb != null) {
            // if we are still in exploration, so the state is IDLE for sure
            machineState = mInfoRb.getState();
        }

        // wait with decision while unconclusive machine state
        int breakCounter = 0;
        while (!machineState.equalsIgnoreCase(IDLE)
                && !machineState.equalsIgnoreCase(READY_AT_OUTPUT)
                && !machineState.equalsIgnoreCase("BROKEN")
                && breakCounter < 100) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.err.println("Sleep was interrupted in failTaskHandler");
            }

            mInfoRb = machineInfoRefBoxDao.findByName(subTask.getMachine());

            if (mInfoRb != null) {
                // if we are still in exploration, so the state is IDLE for sure
                machineState = mInfoRb.getState();
            }

            if (mInfoRb != null && !"DOWN".equalsIgnoreCase(machineState)) {
                breakCounter++;
            }

            System.out.println("Wait for machine " + subTask.getMachine() + " to become IDLE/READY-AT-OUTPUT/BROKEN");
        }
        return machineState;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        List<SubProductionTask> task = subProductionTaskDao.findByMachineAndState(machine.getName(), SubProductionTask.TaskState.SUCCESS_PENDING);
        for (SubProductionTask subProductionTask : task) {
            MachineClientUtils.MachineState state = client.getStateForMachine(MachineClientUtils.parseMachineWithColor(subProductionTask.getMachine())).get();
            if (state.equals(MachineClientUtils.MachineState.READY_AT_OUTPUT)) {
                subProductionTask.setState(SUCCESS);
                //this.handleResourcesOnSuccess(subProductionTask);
                subProductionTaskDao.save(subProductionTask);
            }
        }
    }


    public void assignTaskToRobot (SubProductionTask task, int robotId) {

        SubProductionTask moveTask = SubProductionTaskBuilder.newBuilder()
                .setName("MoveToMachine " + task.getMachine())
                .setMachine(task.getMachine())
                .setState(SubProductionTask.TaskState.INWORK)
                .setType(SubProductionTask.TaskType.MOVE)
                .setSide(task.getSide())
                .setOrderInfoId(null)
                .setFatal(false)
                .setRequiresReset(false)
                .setIsDemandTask(task.isDemandTask())
                .build();

        if (moveTask.getSide() == MachineSide.SHELF || moveTask.getSide() == MachineSide.SLIDE)
            moveTask.setSide(MachineSide.INPUT);

        toExecute[robotId-1] = task;
        moveTask.setRobotId(robotId);
        subProductionTaskDao.save(moveTask);
        robotClientWrapper.sendProductionTaskToRobot(moveTask);

        timerStarting[robotId - 1] = gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000;

    }


    public void assignNonMoveTaskToRobot (SubProductionTask task, int robotId) {
        log.info("TASK TO EXECUTE BY ROBOT ID " + robotId);
        task.setState(SubProductionTask.TaskState.INWORK, "set to INWORK due to assigning task to robot");
        subProductionTaskDao.save(task);
        robotClientWrapper.sendProductionTaskToRobot(task);
    }




    public void cancelCurrentTask (int robotId) {
        robotClient.cancelTask(robotId);
    }




    //DISTANCE MATRIX CALCULATED USING A*
    public void generateDistanceMatrix () {
        distanceMatrix = new HashMap<String, Double>();
        List<MachineInfoRefBox> machinelist = machineInfoRefBoxDao.findByTeamColor(TeamColor.CYAN);

        for(MachineInfoRefBox machine1 : machinelist) {
            for(MachineInfoRefBox machine2 : machinelist) {
                double distance;

                if (machine1.getType().equals("CS")) {
                    if (machine2.getType().equals("BS" )) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {

                        if(! (machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if(! (machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.SHELF);
                            distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_shelf", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        if(! (machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        }
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.SHELF, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_shelf" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("BS")) {
                    if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                }  else if (machine1.getType().equals("RS")) {
                    if (machine2.getType().equals("BS" )) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);

                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        if(! (machine1.getName().equals(machine2.getName()))) {
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.OUTPUT);
                            distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_output", distance);
                            distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                            distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        }
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    } else if (machine2.getType().equals("DS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.OUTPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_output" + machine2.getName() + "_input", distance);
                    }
                } else if (machine1.getType().equals("DS")) {
                    if (machine2.getType().equals("BS" )) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("CS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.SHELF);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_shelf", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                    } else if (machine2.getType().equals("RS")) {
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.OUTPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_output", distance);
                        distance = pathEstimator.pathLength(machine1.getName(), MachineSide.INPUT, machine2.getName(), MachineSide.INPUT);
                        distanceMatrix.put(machine1.getName() + "_input" + machine2.getName() + "_input", distance);
                    }
                }
                //starting point 115/5
                double distancefromstart;
                if (machine1.getType().equals("DS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                }
                else if (machine1.getType().equals("CS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.SHELF);
                    distanceMatrix.put("start" + machine1.getName() + "_shelf", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("RS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.INPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_input", distancefromstart);
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                } else if (machine1.getType().equals("BS")) {
                    distancefromstart = pathEstimator.pathLength(115, 5, machine1.getName(), MachineSide.OUTPUT);
                    distanceMatrix.put("start" + machine1.getName() + "_output", distancefromstart);
                }
            }



        }

    }




    /* DISTANCE MATRIX CALCULATED USING EUCLIDEAN DISTANCE
    public void generateDistanceMatrix () {
        distanceMatrix = new HashMap<String, Double>();
        List<MachineInfoRefBox> machinelist = machineInfoRefBoxDao.findByTeamColor(TeamColor.CYAN);

        //printing machines position
        for(MachineInfoRefBox machine : machinelist) {
            String zone = machine.getZone();
            int zoneXindex = Character.getNumericValue(zone.charAt(3));
            int y = Character.getNumericValue(zone.charAt(4));
            int x;
            char zoneColor = zone.charAt(0);
            if (zoneColor == 'M')
                x = 7 - zoneXindex + 1;
            else
                x = 7 + zoneXindex;
            System.out.println("machine " + machine.getName() + " in position " + x + ";" + y);
        }


        for(MachineInfoRefBox machine1 : machinelist) {
            String zone1 = machine1.getZone();
            for(MachineInfoRefBox machine2 : machinelist) {
                String zone2 = machine2.getZone();
                double distance;
                if (!machine1.getName().equals(machine2.getName())) {
                    distance = Math.sqrt( Math.pow(getXfromZone(zone1)-getXfromZone(zone2),2) +  Math.pow(getYfromZone(zone1)-getYfromZone(zone2),2)) * timeFactor;
                } else {
                    distance = 2; //cost to go from one side of the machine to the other
                }
                distanceMatrix.put(machine1.getName() + machine2.getName(), distance);
            }
            double distancefromstart = Math.sqrt( Math.pow(getXfromZone(zone1)-13,2) +  Math.pow(getYfromZone(zone1)-2,2)) * timeFactor;
            distanceMatrix.put(machine1.getName() + "start", distancefromstart);
            distanceMatrix.put("start" + machine1.getName(), distancefromstart);
        }

        System.out.println("aaa " + distanceMatrix.toString());

    }*/



    public void writeDistanceCheck (FileWriter fr, String from, String to, double cost) throws IOException {
        int seconds = (int) (cost * timeFactor);
        if (!from.equals(to)) {
            fr.write("( = (distance " + from + " " + to + ") " + seconds + ")\n ");
        }
    }

    public void writeDistance (FileWriter fr, String from, String fromCompleteName, MachineInfoRefBox to) throws IOException {
        String nameTo = to.getName();
        if (to.getType().equals("CS")) {
            if (!fromCompleteName.equals(nameTo+ "_input"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) +  "_input", distanceMatrix.get(fromCompleteName + nameTo +  "_input"));
            if (!fromCompleteName.equals(nameTo+ "_output"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"));
            if (!fromCompleteName.equals(nameTo+ "_shelf"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_shelf", distanceMatrix.get(fromCompleteName + nameTo + "_shelf"));
        } else if (to.getType().equals("RS")) {
            if (!fromCompleteName.equals(nameTo+ "_input"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_input", distanceMatrix.get(fromCompleteName + nameTo + "_input"));
            if (!fromCompleteName.equals(nameTo+ "_output"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"));
        } else if (to.getType().equals("BS") && !fromCompleteName.equals(nameTo+ "_output")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_output"));
        } else if (to.getType().equals("DS") && !fromCompleteName.equals(nameTo+ "_input")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_input"));
        }
    }

    public void generateProblemFile (List<ProductOrder> goals) {

        try {
            File file = new File(problemS + ".pddl");
            FileWriter fr = new FileWriter(file, false);
            fr.write("(define (problem task)\n" +
                    "(:domain rcll)\n" +
                    "(:objects\n" +
                    "    start - starting\n" +
                    "    bs - base_station \n" +
                    "    ds - delivery_station\n" +
                    "    cs1_input cs2_input - cap_station_input\n" +
                    "    cs1_output cs2_output - cap_station_output\n" +
                    "    cs1_shelf cs2_shelf - cap_station_shelf\n" +
                    "    rs1_input rs2_input - ring_station_input\n" +
                    "    rs1_output rs2_output - ring_station_output\n" +
                    "    cs1 cs2 - cap_station\n" +
                    "    rs1 rs2 - ring_station\n" +
                    "    r1 r2 r3 - robot\n");
            for(ProductOrder p : goals){
                fr.write("p" + p.getId() + " ");
            }

            fr.write(" - product\n" +
                    "     cap_black cap_grey - ccolor\n" +
                    "    ring_yellow ring_orange ring_blue ring_green - rcolor\n" +
                    ")\n" +
                    "(:init\n");

            kb.prettyPrintKBtoProblemFile(fr);      //printing the Knowledge Base into the problem file
            generateGoalOnProblemFile(goals,fr);


            List<MachineInfoRefBox> machinelist = machineInfoRefBoxDao.findByTeamColor(TeamColor.CYAN);
            for(MachineInfoRefBox machine1 : machinelist) {
                String name1 = machine1.getName();
                for (MachineInfoRefBox machine2 : machinelist) {
                    String name2 = machine2.getName();
                    if (machine1.getType().equals("CS")) {
                        writeDistance(fr, "cs" + name1.charAt(4) + "_input", name1 + "_input", machine2);
                        writeDistance(fr, "cs" + name1.charAt(4) + "_output", name1 + "_output", machine2);
                        writeDistance(fr, "cs" + name1.charAt(4) + "_shelf", name1 + "_shelf", machine2);
                    } else if (machine1.getType().equals("RS")) {
                        writeDistance(fr, "rs" + name1.charAt(4) + "_input", name1 + "_input", machine2);
                        writeDistance(fr, "rs" + name1.charAt(4) + "_output", name1 + "_output", machine2);
                    } else if (machine1.getType().equals("BS")){
                        writeDistance(fr, name1.substring(2).toLowerCase(Locale.ROOT), name1 + "_output", machine2);
                    } else if (machine1.getType().equals("DS")){
                        writeDistance(fr, name1.substring(2).toLowerCase(Locale.ROOT), name1 + "_input", machine2);
                    }
                }
            }

            for (MachineInfoRefBox machine : machinelist) {
                writeDistance(fr, "start", "start", machine);
            }

            fr.write(")\n");


            //here I specifiy the goals. In this showcase, I ask for a base (which is a partial product of stage 1 in my domain)
            fr.write("(:goal (and\n");
            for(ProductOrder p : goals){
                fr.write("(stage_c0_1 p" + p.getId() + ")\n");
            }
            fr.write(")\n" +
                    ")\n" +
                    "(:metric minimize  (total-time) )\n" +
                    "\n" +
                    ")");
            fr.close();

        } catch (IOException e) {
            log.error(e);
        }
    }


    public void readFile () {
        try {
            File file=new File(problemS);
            FileInputStream fis=new FileInputStream(file);     //opens a connection to an actual file
            System.out.println("file content: ");
            int r=0;
            while((r=fis.read())!=-1)
            {
                System.out.print((char)r);      //prints the content of the file
            }

        } catch (IOException e) {
            log.error(e);
        }
    }



    public ImmutablePair<TemporalGraph, CodedProblem> callExtPlanner () {

        //PARSING PHASE
        final StringBuilder strb = new StringBuilder();
        Parser parser = new Parser();
        try {
                parser.parse(domainS,problemS  + ".pddl");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        log.warn("parsed\n");

        //ENCODING PHASE

        final Domain domain = parser.getDomain();
        final Problem problem = parser.getProblem();

        try {

            CodedProblem cp = Encoder.encode(domain, problem);

            //PLANNING PHASE

            Planner pl = new Planner(domainS,problemS + ".pddl",cp);
            pl.plan();

            Set<Double> keys = pl.getPlan().actions().keySet();
            Iterator<Double> itk = keys.iterator();
            while(itk.hasNext()) {
                Set<IntOp> ita = pl.getPlan().getActionSet(itk.next());
                Iterator<IntOp> acs = ita.iterator();
                while(acs.hasNext()){
                    IntOp o = acs.next();
                    o.generateLists();
                }
            }

            if (pl.getPlan().size() == 0) {
                log.warn("Plan not found, problem unsolvable");
                return null;
            }
            else {
                pl.printPlan();
                log.warn("Generating temporal graph \n");
                //plan is represented as a temporal graph inside tg object
                TemporalGraph tg = new TemporalGraph(cp, pl.getPlan(), productOrderDao);
                tg.createTemporalGraph();
                tg.normalizeActions();
                //tg.printOrderedActions();
                //tg.printGraph();
                log.warn("finish");
                return new ImmutablePair<>(tg,cp);
            }



        } catch (IllegalArgumentException ilException) {
            log.error("the problem to encode is not ADL, \":requirements\" not supported at this time\n");
            return null;
        }
    }

    public void dispatch () {
        timer = new Timer();
        timerStarting[0] = gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000;
        timerStarting[1] = gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000;
        timerStarting[2] = gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000;

        //get the plan starting node
        TemporalNode planStart = tgraph.getNode(0);
        tryDispatchNode(planStart);

    }

    public void tryDispatchNode (TemporalNode node) {
        //checking input edges are all enabled
        if (!stop) {
            boolean ready = true;
            Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
            Iterator<TemporalEdge> it = inEdges.iterator();
            while(it.hasNext() && ready){
                TemporalEdge e = it.next();
                if(!e.getEnabled()) {
                    ready = false;
                }
            }
            if (ready && node.getType() != 2) {
                if (node.getType() != 3) {
                    synchronized (this) {
                        SubProductionTask task = actionMapping.actionToTask(tgraph.nodeToAction(node.getTime(), node.getId()));
                        int robotId = task.getRobotId();
                        robotStopped[robotId - 1] = false;
                    }
                }
                //dispatching the node
                dispatchNode(node);
                //enabling outpud edges
                Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                it = outEdges.iterator();
                while (it.hasNext()) {
                    TemporalEdge e = it.next();
                    if (/*e.getLb() == Double.MIN_VALUE && */ true) {              //simplest case, without lower bound on the edge
                        e.enable();
                        tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                        tryDispatchNode(tgraph.getTarget(e));
                    } else {                                        //action duration is the only case for the moment. Otherwise the method run of class ActionEnd at the end of the page has to be implemented
                        timer.schedule(new ActionEnd(e), Math.round(e.getLb() * 100));
                    }
                }

            }
        }



    }

    public void dispatchEndNode (TemporalNode node) {
        if (node.isValid()) {
            boolean ready = true;
            Set<TemporalEdge> inEdges = tgraph.getInputEdges(node);
            Iterator<TemporalEdge> it = inEdges.iterator();
            while (it.hasNext() && ready) {
                TemporalEdge e = it.next();
                if (!e.getEnabled()) {
                    ready = false;
                }
            }
            if (ready) {
                double time = node.getTime();
                IntOp action = tgraph.nodeToAction(time, node.getId());

                SubProductionTask task = actionMapping.actionToTask(action);

                System.out.println("Time: " + time + " Timer: " + gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000 + " - Finishing action " + action);


                //enabling outpud edges
                int robotId = task.getRobotId();
                if (!stop) {
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                    }
                    Set<TemporalEdge> outEdges = tgraph.getOutputEdges(node);
                    it = outEdges.iterator();
                    while (it.hasNext()) {
                        TemporalEdge e = it.next();
                        if (/*e.getLb() == Double.MIN_VALUE && */ true) {              //simplest case, without lower bound on the edge
                            e.enable();
                            tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);
                            tryDispatchNode(tgraph.getTarget(e));
                        } else {                                        //action duration is the only case for the moment. . Otherwise the method run of class ActionEnd at the end of the page has to be implemented
                            timer.schedule(new ActionEnd(e), Math.round(e.getLb() * 10));
                        }
                    }

                    if (robotStopped[0] && robotStopped[1] && robotStopped[2]) {
                        System.out.println("Plan dispatching end, starting to plan for new orders");
                    } else {
                        if (robotStopped[0])
                            System.out.println("robot 1 idling");
                        if (robotStopped[1])
                            System.out.println("robot 2 idling");
                        if (robotStopped[2])
                            System.out.println("robot 3 idling");
                    }

                } else { //receive a signal to stop dispatching, stopping the robot

                    System.out.println("stopping robot " + robotId );
                    synchronized (this) {
                        robotStopped[robotId - 1] = true;
                        if (robotStopped[0] && robotStopped[1] && robotStopped[2])
                            System.out.println("++++++ ALL ROBOT STOPPED!!!!");
                    }
                }
            }
            if (!ready)
                System.out.println("+++++++++++ERROR IN ACTION ENDING+++++");
        }
    }



    public void dispatchNode (TemporalNode node) {
        if (node.isValid()) {
            int type = node.getType();
            double time = node.getTime();
            if (type == 1) {
                IntOp action = tgraph.nodeToAction(time, node.getId());

                System.out.println("Time: " + time + " Timer: " + gameStateDao.getLatestGameTimeProductionPhase().longValue()/1000000000 + " - Dispacting action " + action);
                SubProductionTask task = actionMapping.actionToTask(action);
                int robotId = task.getRobotId();
                executing[robotId - 1] = node;
                assignTaskToRobot(task, robotId);


            } else if (type == 2) { //end action
                System.out.println("error");
            }
        }
    }


    //
    public class ActionEnd extends TimerTask {

        TemporalEdge e;

        public ActionEnd(TemporalEdge e) {
            this.e = e;
        }

        @Override
        public void run() { //implement this part to dispatch nodes through an edge with a high lower bound
            //e.enable();
            //tgraph.updateEdge(tgraph.getSource(e), tgraph.getTarget(e), e);

        }


    }




}
