package com.grips.scheduler.production;

import com.google.common.collect.Lists;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.shared.domain.Complexity;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@CommonsLog
public class SubProductionUtils {

    @Autowired
    SubProductionTaskDao subProductionTaskDao;

    public SubProductionTask getSuccessorDeliverTask(SubProductionTask t) {
        if (t.getType() != SubProductionTask.TaskType.GET && t.getProductTask().getProductOrder().getComplexity() != Complexity.D0) {
            log.error("Task is no GET Task " + t.getId() + " in getSuccessorDeliverTask (" + t.getName() + ")", new RuntimeException("Here!"));
            return null;
        }

        Collection<SubProductionTask> successors = subProductionTaskDao.findByPreConditionTasksIn(Lists.newArrayList(t));
        Optional<SubProductionTask> foundSuccesor = successors.stream().filter(s -> s.getType() == SubProductionTask.TaskType.DELIVER && s.getSameRobotSubTask().getId() == t.getId()).findAny();
        if (foundSuccesor.isPresent()) {
            return foundSuccesor.get();
        }

        return null;
    }


    public SubProductionTask getSuccessorDeliverTask(SubProductionTask t, SubProductionTask.TaskState state) {
        if (t.getType() != SubProductionTask.TaskType.GET && t.getProductTask().getProductOrder().getComplexity() != Complexity.D0) {
            log.error("Task is no GET Task " + t.getId() + " in getSuccessorDeliverTask (" + t.getName() + ")", new RuntimeException("Here!"));
            return null;
        }

        Collection<SubProductionTask> successors = subProductionTaskDao.findByStateAndPreConditionTasksIn(state, Lists.newArrayList(t));
        Optional<SubProductionTask> foundSuccesor = successors.stream()
                .filter(s -> s.getType() == SubProductionTask.TaskType.DELIVER && s.getSameRobotSubTask().getId() == t.getId())
                .findAny();
        if (foundSuccesor.isPresent()) {
            return foundSuccesor.get();
        }

        return null;
    }
}
