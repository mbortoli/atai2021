package com.grips.scheduler.production;

import com.grips.model.teamserver.MachineClient;
import com.grips.persistence.dao.SubProductionTaskDao;
import com.grips.scheduler.api.IScheduler;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SubProductionTaskUpdater {
    @Autowired
    private SubProductionTaskDao subProductionTaskDao;

    @Autowired
    @Qualifier("production-scheduler")
    private IScheduler productionScheduler;
    @Autowired
    private MachineClient client;

    public void updateTaskPerMachine(MachineInfoProtos.MachineInfo info) {
        //todo remove hard coded TEAM value!
        if (info.getTeamColor().equals(TeamProtos.Team.CYAN)) {
            info.getMachinesList().forEach(machine -> {
                productionScheduler.handleMachineInfo(machine);
            });
        }
    }
}
