package com.grips.scheduler.challanges;

import com.grips.model.teamserver.Peer;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.challanges.utils.DataSet;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.NavigationChallengeProtos;
import org.robocup_logistics.llsf_msgs.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.LinkedList;
import java.util.List;

import static com.grips.scheduler.challanges.utils.KMeans.*;

@CommonsLog
public class NavigationScheduler implements IScheduler {

    private NavigationChallengeProtos.NavigationRoutes routes;

    @Override
    public void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot) {
        if (routes == null) {
            log.warn("No routes received yet!");
            return;
        }

    }

    @Override
    public void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {
        log.error("NavigationScheduler::handleBeaconSignal not implemented!");
    }

    public void updateRoutes(NavigationChallengeProtos.NavigationRoutes update) {
        routes = update;
    }

    public Boolean clusterRoutes()
    {
        LinkedList<String> names = new LinkedList<String>();
        names.add("X");
        names.add("Y");

        LinkedList<List<Integer>> values = new LinkedList<List<Integer>>();

        for(int i = 0; i < routes.getRoutesList().get(0).getRouteList().size(); ++i)
        {
            ZoneProtos.Zone zone = routes.getRoutesList().get(0).getRouteList().get(i);
            Integer number = zone.getNumber() - 1000;
            List<Integer> coord = new LinkedList<>();
            coord.add((number - (number % 10)) / 10);
            coord.add(number % 10);
            values.add(coord);
        }

        DataSet data = new DataSet(names, values);

        // remove prior classification attr if it exists (input any irrelevant attributes)
        data.removeAttr("Class");

        // cluster
        for(int i = 0; i < 100; ++i)
        {
            kmeans(data, 3);
            if(data.validate())
                break;
        }

        // output into a csv
        List<List<Integer>> cluster_0 = data.dataToList(0);
        List<List<Integer>> cluster_1 = data.dataToList(1);
        List<List<Integer>> cluster_2 = data.dataToList(2);

        List<List<Double>> graph_0 = new LinkedList<>();
        for(List<Integer> zone1 : cluster_0)
        {
            List<Double> distance = new LinkedList<>();
            for(List<Integer> zone2 : cluster_0)
            {
                distance.add(Math.sqrt(Math.pow(zone2.get(0) - zone1.get(0), 2) + Math.pow(zone2.get(1) - zone1.get(1), 2)));
            }
            graph_0.add(distance);
        }
        int n = cluster_0.size();
        boolean[] v = new boolean[n];
        v[0] = true;
        Double ans = Double.MAX_VALUE;
        List<Integer> zones = new LinkedList<>();
        ans = tsp(graph_0, v, 0, n, 1, 0.0, ans, zones);
        data.createCsvOutput();
        System.out.println(zones);
        System.out.println( n);
        return data.validate();
    }

    public Double tsp(List<List<Double>> graph, boolean[] v, int currPos, int n, int count, Double cost, Double ans, List<Integer> zones)
    {
        if (count == n && graph.get(currPos).get(0) > 0)
        {
            if(ans != Math.min(ans, cost + graph.get(currPos).get(0)))
                zones.add(currPos);
            ans = Math.min(ans, cost + graph.get(currPos).get(0));
            return ans;
        }

        for (int i = 0; i < n; i++)
        {
            if (v[i] == false && graph.get(currPos).get(i) > 0)
            {

                // Mark as visited
                v[i] = true;
                ans = tsp(graph, v, i, n, count + 1,
                        cost + graph.get(currPos).get(i), ans, zones);

                // Mark ith node as unvisited
                v[i] = false;
            }
        }
        return ans;
    }
}
