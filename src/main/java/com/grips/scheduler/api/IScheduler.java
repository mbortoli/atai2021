package com.grips.scheduler.api;

import com.grips.model.teamserver.Peer;
import org.robocup_logistics.llsf_msgs.GripsBeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

public interface IScheduler {
    void handleBeaconSignal(GripsBeaconSignalProtos.GripsBeaconSignal beacon_signal, int robotId, Peer robot);
    void handleRobotTaskResult(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask);
    void handleMachineInfo(MachineInfoProtos.Machine machine);
}
