/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.scheduled_tasks;

import com.grips.persistence.dao.GameStateDao;
import com.grips.protobuf_lib.RobotConnections;
import com.robot_communication.services.RobotClient;
import com.robot_communication.services.PrsTaskCreator;
import com.shared.domain.GamePhase;
import org.apache.commons.lang3.StringUtils;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service

public class TriggerReportAllMachinesTask {

    private final RobotClient robotClient;
    private final RobotConnections robotConnections;
    private final PrsTaskCreator prsTaskCreator;
    private final GameStateDao gameStateDao;

    public TriggerReportAllMachinesTask(RobotClient robotClient,
                                        RobotConnections robotConnections,
                                        PrsTaskCreator prsTaskCreator,
                                        GameStateDao gameStateDao) {
        this.robotClient = robotClient;
        this.robotConnections = robotConnections;
        this.prsTaskCreator = prsTaskCreator;
        this.gameStateDao = gameStateDao;
    }


    //@Scheduled(cron = "${task.triggerReportAllMachines}")
    public void run() {
        if (gameStateDao.getGamePhase() != GamePhase.SETUP
                && gameStateDao.getGamePhase() != GamePhase.EXPLORATION) {
            return;
        }

        String teamColor = gameStateDao.getTeamColor();
        if (StringUtils.isEmpty(teamColor)) {
            System.err.println("teamColor is empty!");
            return;
        }
        for (Long robot : robotConnections.getclientId()) {
            GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask = prsTaskCreator.createReportAllMachinesTask(robot);
            robotClient.sendToRobot(robot, prsTask);
        }
    }
}
