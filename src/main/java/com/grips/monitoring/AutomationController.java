package com.grips.monitoring;

import com.grips.persistence.dao.GameStateDao;
import com.shared.domain.GamePhase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigInteger;
import java.util.Optional;

@RestController
public class AutomationController {

    @Autowired
    private GameStateDao gameStateDao;

    @RequestMapping("/game/phase")
    public String getGamePhase() {
        return Optional.ofNullable(gameStateDao.getGamePhase().name()).orElse("NULL");
    }

    @RequestMapping("/game/time")
    public BigInteger gameTime() {
        if (gameStateDao.getGamePhase().equals(GamePhase.EXPLORATION)) {
            return Optional.ofNullable(gameStateDao.getLatestGameTimeExplorationPhase()).orElse(BigInteger.ZERO);
        } else {
            return Optional.ofNullable(gameStateDao.getLatestGameTimeProductionPhase()).orElse(BigInteger.ZERO);
        }
    }

    @RequestMapping("/game/state")
    public String getGameState() {
        return Optional.ofNullable(gameStateDao.getGameState().name()).orElse("NULL");
    }
}
