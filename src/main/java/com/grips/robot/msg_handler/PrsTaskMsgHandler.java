package com.grips.robot.msg_handler;

import com.shared.domain.MachineName;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class PrsTaskMsgHandler implements Consumer<GripsMidlevelTasksProtos.GripsMidlevelTasks> {

    private final GameField gameField;
    private final ExplorationScheduler explorationScheduler;
    private final IScheduler productionScheduler;

    public PrsTaskMsgHandler(GameField gameField,
                             ExplorationScheduler explorationScheduler,
                             @Qualifier("production-scheduler") IScheduler productionScheduler) {
        this.gameField = gameField;
        this.explorationScheduler = explorationScheduler;
        this.productionScheduler = productionScheduler;
    }

    @Override
    public void accept(GripsMidlevelTasksProtos.GripsMidlevelTasks prsTask) {
        if (prsTask.hasExploreMachine()) {
            //System.out.println("Received PrsTask with exploreMachineAtWaypointTask.");
            log.info("Received exploreMachineAtWaypointTaskResult from Robot " + prsTask.getRobotId() + " Zone was: " +
                    prsTask.getExploreMachine().getWaypoint() + " Machine was: " + prsTask.getExploreMachine().getMachineId());
            String zoneName = prsTask.getExploreMachine().getWaypoint();
            if (null == zoneName) {
                System.err.println("Error parsing zone!");
                return;
            }

            // Update Zone with exploration state
            if (prsTask.getSuccessful()) {
                log.info("Successfully explored zone " + prsTask.getExploreMachine().getWaypoint());

                gameField.getZoneByName(zoneName).setExplorationState(ExplorationZone.ZoneExplorationState.EXPLORED);

            } else {
                log.info("Exploration at zone " + prsTask.getExploreMachine().getWaypoint() + " for machine " + prsTask.getExploreMachine().getMachineId() + " failed!");

                explorationScheduler.explorationTaskFailed(prsTask.getRobotId(), new MachineName(prsTask.getExploreMachine().getMachineId()), prsTask.getExploreMachine().getWaypoint());
            }
        } else if (prsTask.hasDeliverToStation() || prsTask.hasGetFromStation() || prsTask.hasMoveToWaypoint() || prsTask.hasBufferCapStation()) {
            productionScheduler.handleRobotTaskResult(prsTask);
        } else {
            log.error("Unknown task result received: " + prsTask.toString());
        }

    }
}
