package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "gameconfig.productionpoints")
@Getter
@Setter
public class ProductionPointsConfig {
    private int bufferBaseInRS;
    private int bufferCapInCS;
    private int finishCC0;
    private int finishCC1;
    private int finishCC2;
    private int lastRingC1;
    private int lastRingC2;
    private int lastRingC3;
    private int mountCap;
    private int inTimeDelivery;
    private int lateDelivery;
}
