package com.grips.refbox.msg_handler;

import com.grips.model.teamserver.TeamColor;
import com.grips.persistence.dao.GameStateDao;
import com.grips.persistence.domain.GameState;
import com.grips.config.Config;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GameStateProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@CommonsLog
@Service
public class GameStateHandler implements Consumer<GameStateProtos.GameState> {

    private final Mapper mapper;
    private final GameStateDao gameStateDao;

    public GameStateHandler(Mapper mapper, GameStateDao gameStateDao) {
        this.mapper = mapper;
        this.gameStateDao = gameStateDao;
    }

    @Override
    public void accept(GameStateProtos.GameState gameState) {
        GameState state = mapper.map(gameState, GameState.class);
        if (state.getTeamCyan().equalsIgnoreCase(Config.TEAM_NAME)) {
            state.setGripsColor(TeamColor.CYAN);
        }
        else if (state.getTeamMagenta().equalsIgnoreCase(Config.TEAM_NAME)) {
            state.setGripsColor(TeamColor.MAGENTA);
        }
        else {
            log.error("NO TEAM IS MATCHING OUR TEAM NAME, CANNOT ASSIGN TEAMCOLOR");
        }
        gameStateDao.save(state);
    }
}
