package com.grips.refbox.msg_handler;

import com.grips.persistence.domain.Ring;
import com.grips.persistence.dao.RingDao;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.RingInfoProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class RingInfoHandler implements Consumer<RingInfoProtos.RingInfo> {

    private final Mapper mapper;
    private final RingDao ringDao;

    public RingInfoHandler(Mapper mapper, RingDao ringDao) {
        this.mapper = mapper;
        this.ringDao = ringDao;
    }

    @Override
    public void accept(RingInfoProtos.RingInfo ringInfo) {
        for (RingInfoProtos.Ring tmp_ring : ringInfo.getRingsList()) {
            Ring ring = mapper.map(tmp_ring, Ring.class);
            ringDao.save(ring);
        }
    }
}
