package com.grips.refbox.msg_handler;

import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.challanges.NavigationScheduler;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.NavigationChallengeProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class NavigationChallangeHandler implements Consumer<NavigationChallengeProtos.NavigationRoutes> {

    private final NavigationScheduler navScheduler;

    @Autowired
    public NavigationChallangeHandler(@Qualifier("production-scheduler") IScheduler scheduler) {
        if (scheduler instanceof NavigationScheduler) {
            navScheduler = (NavigationScheduler) scheduler;
        } else {
            navScheduler = null;
        }
        log.warn("Going to publish fake route!");
    }

    @Override
    public void accept(NavigationChallengeProtos.NavigationRoutes navigationRoutes) {
        log.info("Got navigationoRoutes: " + navigationRoutes.toString());
    }
}
