RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color
cd $TEAM_SERVER_DIR
echo -e "Going to use : ${RED}$GRIPS_HW_TYPE${NC} as tools image!"
if [ -z "$GRIPS_HW_TYPE" ]; then
  echo -e "${RED}GRIPS_HW_TYPE not set, please set it via 'export GRIPS_HW_TYPE=<value>'. Possible values: intel/nvidia"
  echo -e "Add it to your bashrc and then try agin. You have to either source your bashrc or start a new terminal.${NC}"  
  exit
fi
if [[ $# -eq 0 ]]
then
  echo -e "${RED}Missing argument, usage './start <number-of-robots>' where number of robots can be 0, 1, 2 or 3.${NC}. If 0 is selected, only the teamserver and the database are started!"
  exit 
fi
xhost local:root
docker rm refbox
docker rm grips_db
docker rm grips_ros_core
docker rm grips_team_server
docker rm grips_gazebo
docker rm grips_simulation
docker rm grips_simulation_2
docker rm grips_simulation_3
scripts/bash/stop.sh
set -e
./gradlew build
docker build . -f scripts/docker/Dockerfile -t $TS_TEAM_SERVER_IMAGE
if [[ $1 == 0 ]]
then
  docker-compose -f scripts/compose/docker-compose-standalone.yml up --force-recreate
else
  logisticsDir="/home/$USER/logistics"
  if [ "$GRIPS_SKIP_LOCAL_BUILD" = false ]; then
    if [ -d "$logisticsDir" ]; then
      # Take action if $DIR exists. #
      echo -e "${YELLOW}Found logistics folder, building local code!${NC}"
      cd $LOGISTICS_FOLDER/InstallScrips/docker;
      docker build . -f Dockerfile.ros1_build --build-arg USER_NAME=$USER -t $SIM_BASE_IMG --target $GRIPS_HW_TYPE;
      cd $LOGISTICS_FOLDER;
      docker build . -f InstallScrips/docker/Dockerfile.simulation --build-arg SIM_BASE_IMG=$SIM_BASE_IMG -t $SIM_IMG
      cd $TEAM_SERVER_DIR
    else
      echo -e "${YELLOW}WARNING: logistics folder not found!${NC}"
    fi
  else
    echo -e "${YELLOW}Skipping local build, pulling from server!${NC}"
#    docker pull $TS_LOGISTICS_IMAGE
#    docker pull registry.gitlab.com/grips_robocup/logistics/tools:intel
#    docker pull registry.gitlab.com/grips_robocup/logistics/tools:nvidia
  fi
  if [[ $1 == 1 ]]
  then
    docker-compose -f scripts/compose/docker-compose-1.yml up --force-recreate
  else
    if [[ $1 -eq 2 ]]
    then
      docker-compose -f scripts/compose/docker-compose-2.yml up --force-recreate
    else
      if [[ $1 -eq 3 ]]
      then
        docker-compose -f scripts/compose/docker-compose-3.yml up --force-recreate
      else
        if [[ $1 -eq 0 ]]
        then
          docker-compose -f scripts/compose/docker-compose-0.yml up --force-recreate
        fi
      fi
    fi
  fi
fi
