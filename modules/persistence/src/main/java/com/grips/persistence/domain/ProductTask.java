package com.grips.persistence.domain;

import com.visualization.domain.VisualizationTask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class ProductTask implements VisualizationTask {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

    @Enumerated(EnumType.STRING)
	private ProductState state;

    @OneToMany(mappedBy = "productTask", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Collection<SubProductionTask> subProductionTasks;

    @OneToOne
	private ProductOrder productOrder;

    @Lob
    private String stateLogging;

    public void setState(ProductState state, String stateLogging) {
        this.state = state;
        this.stateLogging += ("; " + stateLogging);
    }
    public enum ProductState {
	    TBD, INWORK, FINISHED, FAILED
    }
}
