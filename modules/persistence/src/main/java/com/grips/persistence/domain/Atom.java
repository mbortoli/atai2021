package com.grips.persistence.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Atom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean isPredicate;   //true -> predicate   //false -> function

    private String name;

    private String attributes;   //attribute names separated by a comma

    private double result; //result of a function

    public Atom () {

    }

    public Atom(String name, String attributes) {
        super();
        this.isPredicate = true;
        this.name = name;
        this.attributes = attributes;
    }

    public Atom(String name, String attributes, double result) {
        super();
        this.isPredicate = false;
        this.name = name;
        this.attributes = attributes;
        this.result = result;
    }

    public Atom(Atom at) {
        super();
        this.isPredicate = at.isPredicate;
        this.name = at.name;
        this.attributes = at.attributes;
        this.result = at.result;
    }


    public double getResult () {
        return this.result;
    }



    public String toString () {
        return (name + " " + attributes);
    }

    public String prettyPrinter () {
        String s = "";
        if (!isPredicate){
            s += "(= ";
            s += "(" + name;
        }
        else
            s += "(" + name;
        String [] attributesArray = attributes.split(";");
        for(int i = 0; i < attributesArray.length; i++)
            s += " " + attributesArray[i];
        s += ")";
        if (!isPredicate){
            s += " " + result + ")";
        }
        s += "\n";
        return s;
    }
}
