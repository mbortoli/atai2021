package com.grips.persistence.domain;

import com.visualization.domain.VisualizationLock;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class LockPart implements VisualizationLock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Long productId;

    private String machine;

    public boolean isLocked() {
        return productId != null;
    }

    public void lock(Long productId) {
        this.productId = productId;
    }

    public void unlock() {
        this.productId = null;
    }
}
