package com.robot_communication.services;

import com.shared.domain.MachineName;
import com.shared.domain.MachineSide;
import lombok.NonNull;
import org.robocup_logistics.llsf_msgs.GripsMidlevelTasksProtos;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;

import java.util.concurrent.ThreadLocalRandom;

public class PrsTaskCreator {
    public PrsTaskCreator() {
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createWaitingTask(@NonNull Long robotId,
                                                                         @NonNull String waitingZoneName) {
        GripsMidlevelTasksProtos.MoveToWaypoint moveToWayPointTask = GripsMidlevelTasksProtos.MoveToWaypoint.newBuilder()
                .setWaypoint(waitingZoneName + "_waiting")
                .build();
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE))
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setMoveToWaypoint(moveToWayPointTask).build();
    }

    //THIS IS NOT NEEDED IN THE MSG!
    private TeamProtos.Team getTeamColor() {
        return TeamProtos.Team.CYAN;
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createMoveToWaypointTask(@NonNull Long robotId,
                                                                                @NonNull Integer taskId,
                                                                                @NonNull String waypoint) {
        GripsMidlevelTasksProtos.MoveToWaypoint moveToWayPointTask = GripsMidlevelTasksProtos.MoveToWaypoint.newBuilder()
                .setWaypoint(waypoint)
                .build();

        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(taskId)
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setMoveToWaypoint(moveToWayPointTask).build();
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createMoveToMachineTask(@NonNull Long robotId,
                                                                               @NonNull Integer taskId,
                                                                               @NonNull MachineName machine,
                                                                               @NonNull MachineSide side) {

        return this.createMoveToWaypointTask(robotId, taskId, machine.getRawMachineName() + side.toString().toLowerCase() + "_move_base");
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createDummyTask(@NonNull Long robotId,
                                                                       @NonNull String machine,
                                                                       MachineSide side) {
        GripsMidlevelTasksProtos.MoveToWaypoint moveToWayPointTask = GripsMidlevelTasksProtos.MoveToWaypoint.newBuilder()
                .setWaypoint(machine + side.toString().toLowerCase() + "_exploration")
                .build();

        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(new Long(System.currentTimeMillis()).intValue())
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setMoveToWaypoint(moveToWayPointTask).build();

    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createExplorationTask(@NonNull Long robotId,
                                                                             String machineId,
                                                                             String zoneId,
                                                                             String side) {
        GripsMidlevelTasksProtos.ExploreMachine exploreTask = GripsMidlevelTasksProtos.ExploreMachine.newBuilder()
                .setMachineId(machineId)
                .setMachinePoint(side)
                .setWaypoint(zoneId).build();

        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE))
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setExploreMachine(exploreTask).build();
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createReportAllMachinesTask(@NonNull Long robotId) {
        /*
        GripsMidlevelTasksProtos.ReportAllSeenMachines reportAllMachinesTask = GripsMidlevelTasksProtos.ReportAllSeenMachines.newBuilder()
                .setReport(true).build();

        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(0)
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setReportAllSeenMachines(reportAllMachinesTask).build();
         */
        throw new RuntimeException("Not supported task!");
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createCancelTask(@NonNull Long robotId) {
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE))
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                .setCancelTask(true).build();
    }


    public GripsMidlevelTasksProtos.GripsMidlevelTasks createStopTask(@NonNull Long robotId,
                                                                      boolean stop) {
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setTaskId(0)
                .setTeamColor(getTeamColor())
                .setRobotId(robotId.intValue())
                //.setStopRobot(stop) //todo add stop in proto msgs!
                .build();
    }

    //todo refactor and replace magic String constants with ENUMs
    public GripsMidlevelTasksProtos.GripsMidlevelTasks createGetWorkPieceTask(@NonNull Long robotId,
                                                                              @NonNull Long taskId,
                                                                              @NonNull String machine,
                                                                              MachineSide side,
                                                                              String shelfslide) {
        if (shelfslide != null) {
            return createGetWorkPieceTaskNew(robotId, taskId, machine, shelfslide);
        } else {
            String providingType = convertSideToprovidingType(side);
            return createGetWorkPieceTaskNew(robotId, taskId, machine, providingType);
        }
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createGetWorkPieceTaskNew(@NonNull Long robotId,
                                                                                 @NonNull Long taskId,
                                                                                 @NonNull String machine,
                                                                                 @NonNull String machinePoint) {
        GripsMidlevelTasksProtos.GetFromStation getWorkPieceTask = GripsMidlevelTasksProtos.GetFromStation.newBuilder()
                .setMachineId(machine)
                .setMachinePoint(machinePoint)
                .build();
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setGetFromStation(getWorkPieceTask)
                .setRobotId(robotId.intValue())
                .setTeamColor(getTeamColor())
                .setTaskId(taskId.intValue()).build();
    }

    //todo refactor and replace magic String constants with ENUMs
    public GripsMidlevelTasksProtos.GripsMidlevelTasks createDeliverWorkPieceTask(@NonNull Long robotId,
                                                                                  @NonNull Long taskId,
                                                                                  @NonNull String machine,
                                                                                  MachineSide side,
                                                                                  String shelfslide) {
        if (shelfslide != null) {
            return createDeliverWorkPieceTaskNew(robotId, taskId, machine, shelfslide);
        } else {
            String providingType = convertSideToprovidingType(side);
            return createDeliverWorkPieceTaskNew(robotId, taskId, machine, providingType);
        }
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createDeliverWorkPieceTaskNew(@NonNull Long robotId,
                                                                                     @NonNull Long taskId,
                                                                                     @NonNull String machine,
                                                                                     @NonNull String machinePoint) {
        GripsMidlevelTasksProtos.DeliverToStation deliverWorkPieceTask = GripsMidlevelTasksProtos.DeliverToStation.newBuilder()
                .setMachineId(machine)
                .setMachinePoint(machinePoint)
                .build();
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setDeliverToStation(deliverWorkPieceTask)
                .setRobotId(robotId.intValue())
                .setTeamColor(getTeamColor())
                .setTaskId(taskId.intValue()).build();
    }

    public GripsPrepareMachineProtos.GripsPrepareMachine createPrepareMachineTask(@NonNull Long robotId,
                                                                                  @NonNull String machineId,
                                                                                  @NonNull String side) {
        return GripsPrepareMachineProtos.GripsPrepareMachine.newBuilder()
                .setRobotId(robotId.intValue())
                .setMachineId(machineId)
                .setMachinePrepared(true)
                .setMachinePoint(side)
                .build();
    }

    public GripsMidlevelTasksProtos.GripsMidlevelTasks createBufferCapTask(@NonNull Long robotId, @NonNull Integer taskId,
                                                                           MachineName machineName, Integer shelf) {
        GripsMidlevelTasksProtos.BufferCapStation bufferCapTask = GripsMidlevelTasksProtos.BufferCapStation.newBuilder()
                .setMachineId(machineName.getRawMachineName())
                .setShelfNumber(shelf)
                .build();
        return GripsMidlevelTasksProtos.GripsMidlevelTasks.newBuilder()
                .setBufferCapStation(bufferCapTask)
                .setRobotId(robotId.intValue())
                .setTeamColor(getTeamColor())
                .setTaskId(taskId).build();
    }

    //todo check with jakob!
    private String convertSideToprovidingType(MachineSide side) {
        switch (side) {
            case OUTPUT:
                return "output";
            default:
                return "input";
        }
    }


}
