package com.robot_communication.services;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;
import org.robocup_logistics.llsf_tools.ReportMachine;

import java.util.AbstractMap;
import java.util.function.Consumer;

@CommonsLog
public class HandleRobotMessageThread extends Thread {

    private final Consumer<GripsBeaconSignalProtos.GripsBeaconSignal> beaconMsgHandler;
    private final Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> prepareMachineMsgHandler;
    private final Consumer<GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines> reportAllMachinesMsgHandler;
    private final Consumer<GripsMidlevelTasksProtos.GripsMidlevelTasks> prsTaskMsgHandler;
    private final Consumer<GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine> machineOrientationMsgHandler;

    private AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg;

    public HandleRobotMessageThread(Consumer<GripsBeaconSignalProtos.GripsBeaconSignal> beaconMsgHandler,
                                    Consumer<GripsPrepareMachineProtos.GripsPrepareMachine> prepareMachineMsgHandler,
                                    Consumer<GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines> reportAllMachinesMsgHandler,
                                    Consumer<GripsMidlevelTasksProtos.GripsMidlevelTasks> prsTaskMsgHandler,
                                    Consumer<GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine> machineOrientationMsgHandler,
                                    AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        this.beaconMsgHandler = beaconMsgHandler;
        this.prepareMachineMsgHandler = prepareMachineMsgHandler;
        this.reportAllMachinesMsgHandler = reportAllMachinesMsgHandler;
        this.prsTaskMsgHandler = prsTaskMsgHandler;
        this.machineOrientationMsgHandler = machineOrientationMsgHandler;
        this.msg = msg;
    }

    @Override
    public void run() {
        handleMsg(this.msg);
    }

    private void handleMsg(AbstractMap.SimpleEntry<GeneratedMessageV3, byte[]> msg) {
        try {
            if (msg.getKey() instanceof GripsBeaconSignalProtos.GripsBeaconSignal) {
                beaconMsgHandler.accept(GripsBeaconSignalProtos.GripsBeaconSignal.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsPrepareMachineProtos.GripsPrepareMachine) {
                prepareMachineMsgHandler.accept(GripsPrepareMachineProtos.GripsPrepareMachine.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines) {
                reportAllMachinesMsgHandler.accept(GripsMachineHandlingReportAllSeenMachinesProtos.GripsMachineHandlingReportAllSeenMachines.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsMidlevelTasksProtos.GripsMidlevelTasks) {
                prsTaskMsgHandler.accept(GripsMidlevelTasksProtos.GripsMidlevelTasks.parseFrom(msg.getValue()));
            } else if (msg.getKey() instanceof GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine) {
                machineOrientationMsgHandler.accept(GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine.parseFrom(msg.getValue()));
            } else {
                log.error("Unknown message in RobotHandler! " + msg.getKey().getClass().getSimpleName());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("Can't parse msg in RobotHandler!", e);
        }
    }

}
