package com.visualization;

import com.visualization.domain.VisualizationBeaconSignal;
import com.visualization.domain.VisualizationExplorationZone;
import com.visualization.domain.VisualizationGameState;
import com.visualization.domain.VisualizationLock;
import com.visualization.domain.VisualizationOrder;
import com.visualization.domain.VisualizationRing;
import com.visualization.domain.VisualizationTask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TeamServerVisualization {
    private VisualizationGameState gameState;
    private long timeStamp;
    private List<? extends VisualizationTask> activeProductTasks;
    private List<? extends VisualizationLock> activePartDemands;
    private List<? extends VisualizationLock> activeLockParts;
    private List<? extends VisualizationBeaconSignal> robotBeaconSignals;
    private List<? extends VisualizationExplorationZone> explorationZones;
    private List<? extends VisualizationOrder> productOrders;
    private List<? extends VisualizationRing> rings;
}
