package com.visualization.domain;

import java.util.Collection;

public interface VisualizationTask {
    Collection<? extends VisualizationSubTask> getSubProductionTasks();
}
